export default {
  apiUrl: {
    baseUrl: "https://www.googleapis.com/youtube/v3/search?",
    part: "snippet",
    type: "video",
    order: String || Number || "",
    maxResults: String || Number || "",
    q: "",
    key: "AIzaSyBdgAYC6bjc0U90msoR15V03nBfvj6MN9o",
  },
};