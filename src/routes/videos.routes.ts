import { Request, Response } from "express";

import { Router } from "express";

import { searchVideos } from "../controllers/videos.controller"

const router = Router();

router.get('/search-videos/:keyword/:results', searchVideos);

export default router;