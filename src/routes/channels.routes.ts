import { Request, Response } from "express";

import { Router } from "express";

import { searchChannels } from "../controllers/channels.controller";

const router = Router();

router.get("/search-channels/:keyword/:results", searchChannels);

export default router;
