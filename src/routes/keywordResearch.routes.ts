import { Request, Response } from "express";

import { Router } from "express";

import { keywordResearch } from '../controllers/seo/keywordResearch.controller'

const router = Router();

router.get('/search-keywords/:keyword/:country/:lang', keywordResearch);

export default router;