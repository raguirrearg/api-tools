import { Request, Response } from "express";
import https from "https";
import { JSDOM } from "jsdom";
import axios from "axios";

/* import configYoutube from "../config/configYoutube";
import configYoutubeStatistics from "../config/configYoutubeStatistics";

const resultStatistics = configYoutubeStatistics.apiStatistics.maxResultsStatistics; */

export const searchVideos = async (req:Request, res:Response) => {

  const filter = null;

  let vid = "EgIQAQ%253D%253D";

  const request = https.request(`https://www.youtube.com/results?search_query=${req.params.keyword}&sp=${ filter ? filter : vid }`,

    (response) => {
      let data = "";
      response.on("data", (chunk) => {
        data = data + chunk.toString();
      });

      response.on("end", async () => {

        const body = data;

        let parser = new JSDOM(body);
        let document = parser.window.document;
        let rawJS = "";

        Object.values(document.querySelectorAll("script")).map((script) => {
          if (script.innerHTML.includes("var ytInitialData")) {
            rawJS = script.innerHTML;
          }
        });

        let r = new Function(rawJS + `return ytInitialData;`);

        let videolist = r().contents.twoColumnSearchResultsRenderer.primaryContents.sectionListRenderer.contents[0].itemSectionRenderer.contents;
        videolist = Object.values(videolist);

        let finalList:any = [];

        /* await Promise.all(videolist.map(async (v:any) => {

            let vi = v;

            if (v.videoRenderer?.videoId) {
              vi.videoRenderer.isMonetized = await isMonetized("https://www.youtube.com/watch?v=" + vi.videoRenderer.videoId);
              vi.videoRenderer.isMonetized = data[0];
              vi.videoRenderer.likes = data[1];
              finalList.push(vi);
            }
          })

        ); */

        await Promise.all(videolist.map(async (v:any) => {

            let vi = v;

            if (v.videoRenderer?.videoId) {

              let data:any = await isMonetized("https://www.youtube.com/watch?v=" + vi.videoRenderer.videoId);

              vi.videoRenderer.isMonetized = data[0];
              vi.videoRenderer.likes = data[1];
              finalList.push(vi);

            }

          })
        );

        res.send(finalList);
      });
    }
  );

  request.on("error", (error) => {
    res.send(400)
  });

  request.end();
  
    
} 

const isMonetized = async (url:any) => {

  return new Promise((resolve, reject) => {

    const request = https.request(url, (response) => {

      let data = "";

      response.on("data", (chunk) => {
        data = data + chunk.toString();
      });

      response.on("end", () => {
        
        const body = data;
        let parser = new JSDOM(body);
        let document = parser.window.document;
        let rawJS = "";

        Object.values(document.querySelectorAll("script")).map((script) => {
          if (script.innerHTML.includes("var ytInitialData")) { rawJS = script.innerHTML; }
        });

        let res = new Function(
          rawJS +`return ytInitialData;`
        );

        let subs = res().contents.twoColumnWatchNextResults.results.results.contents[1].videoSecondaryInfoRenderer.owner.videoOwnerRenderer.subscriberCountText?.simpleText;

        if (subs == undefined) {
          subs = 0;
        } else {
          subs.split(" ")[0];
        }

        let views = res().contents.twoColumnWatchNextResults.results.results.contents[0].videoPrimaryInfoRenderer.viewCount.videoViewCountRenderer.viewCount ?.simpleText;

        if (views == undefined) {
          views = 0;
        } else {
          views.split(" ")[0];
        }

        let likes = res().contents.twoColumnWatchNextResults.results.results.contents[0].videoPrimaryInfoRenderer.videoActions.menuRenderer.topLevelButtons[0].toggleButtonRenderer.defaultText?.simpleText;
        likes = likes ? likes : 0;

        console.log('likes', likes);
        

        if (likes != 0) {
          if (likes.includes(",")) {
            likes = parseInt(likes.replace(",", "")) * 10000;
          } else {
            likes = parseInt(likes.replace(".", ""));
          }
        }

        if (subs != 0) {
          if (subs.includes(",")) {
            subs = parseInt(subs.replace(",", "")) * 10000;
          } else if (subs.includes(".")) {
            subs = parseInt(subs.replace(".", ""));
          } else {
            subs = parseInt(subs);
          }
        }

        if (views != 0) {
          if (views.includes(",")) {
            views = parseInt(views.replace(",", "")) * 10000;
          } else if (views.includes(".")) {
            views = parseInt(views.replace(".", ""));
          } else {
            views = parseInt(views);
          }
        }

        let monteized = false;

        if (data.includes("ads") && views >= 4000 && subs >= 1000) {
          monteized = true;
        }
        resolve([monteized, likes]);

      });
    });

    request.on("error", (error) => {
      reject(new Error());
    });

    request.end();
  });
};

// TODO: Solo para tener en cuenta, api de youtube

/* 

const search = req.params.keyword;
    const results = req.params.results;
    const resultStatistics = req.params.results;

    const { baseUrl, part, type, order, maxResults, q, key } = configYoutube.apiUrl;
    const apiUrlVideos = `${baseUrl}part=${part}&type=${type}&q=${search}&maxResults=${results}&key=${key}`;

    const responseVideos = await axios
      .get(apiUrlVideos)
      .then((res) => {
        const nextPage = res.data.nextPageToken;
        const prevPage = res.data.prevPageToken;

        const responseVideos = res.data.items;

        let textConcatVideosIds = "";

        for (let i = 0; i < responseVideos.length; i++) {
          textConcatVideosIds += responseVideos[i].id.videoId + ",";
        }

        configYoutubeStatistics.apiStatistics.id = textConcatVideosIds;

        const {
          baseUrlStatistics,
          partStatistics,
          id,
          maxResultsStatistics,
          keyStatistics,
        } = configYoutubeStatistics.apiStatistics;
        const urlStatistics = `${baseUrlStatistics}part=${partStatistics}&id=${id}&maxResults=${resultStatistics}&key=${keyStatistics}`;

        axios
          .get(urlStatistics)
          .then((ress) => {
            const responseStatistics = ress.data.items;
            response.send(responseStatistics);
          })
          .catch((error) => console.log(error));
      })
      .catch((error) => console.log(error));


*/